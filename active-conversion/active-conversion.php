<?php
/**
 * Plugin Name: Active Conversion
 * Plugin URI: https://www.activeconversion.com/
 * Description: Active Conversion plugin allows to add tracking code to your pages and also create forms and add them to your pages.
 * Version: 2.0.0
 * Author: Active Conversion
 * Author URI: https://www.activeconversion.com/
 **/

define('AC_PLUGIN_VERSION', '1.0.0');
define( 'AC_PLUGIN_URL', plugin_dir_path( __FILE__ ) );
define( 'AC_PLUGIN_FOLDER_NAME', plugin_dir_path( __DIR__ ) );


define('AC_CODE_PATH', '/ac_code.txt');
define('AC_FORM_CODE', "<!-- START OF ACTIVECONVERSION FORM CODE -->\n" . "<div id=\"acForm_%s\"></div>\n" . "<!-- END OF ACTIVECONVERSION FORM CODE -->");

/************************************************************************************
* a2design addon section begin
*************************************************************************************/

/**
* Debug tool
*/
if ( ! function_exists( 'pr') ) {
    function pr( $val ) {
        echo '<pre class="debug-tool">';
        print_r( $val );
        echo '</pre>';
    }
}
if ( ! function_exists( 'pra') ) {
    function pra( $val ) {
        echo '<pre class="debug-tool">';
        print_r( $val, true );
        echo '</pre>';
    }
}

// Plugin Functions
require_once( AC_PLUGIN_URL . 'includes/plugin-functions.php' );
// Products functions
require_once( AC_PLUGIN_URL . 'includes/products/products-columns.php' );
// Taxonomies custom fields
require_once( AC_PLUGIN_URL . "includes/taxonomies/ac-category-columns.php" );
require_once( AC_PLUGIN_URL . "includes/taxonomies/ac-brochures-specsheets-columns.php" );
require_once( AC_PLUGIN_URL . 'includes/taxonomies/taxonomy-fields.php' );
require_once( AC_PLUGIN_URL . 'includes/taxonomies/save-taxonomies-fields.php' );
require_once( AC_PLUGIN_URL . 'includes/taxonomies/ajax-sorting-terms-order.php' );
// Metaboxes structure
require_once( AC_PLUGIN_URL . 'includes/metaboxes/taxonomy-box/taxonomy-metabox-view.php' );
require_once( AC_PLUGIN_URL . 'includes/metaboxes/taxonomy-box/taxonomy-metabox-ajax.php' );
require_once( AC_PLUGIN_URL . 'includes/metaboxes/images-box/images-metabox-view.php' );
require_once( AC_PLUGIN_URL . 'includes/metaboxes/post-details/post-details-metabox-view.php' );
// Recreate metaboxes
require_once( AC_PLUGIN_URL . 'includes/metaboxes/recreate-metaboxes.php' );
// Update post in admin panel with metaboxes fields like meta_key -> meta_value
require_once( AC_PLUGIN_URL . "includes/metaboxes/post-update-after-all.php" );
// TiniMCE addon
require_once( AC_PLUGIN_URL . "includes/tinimce/global.php" );
// Admin search results page
require_once( AC_PLUGIN_URL . "includes/admin-search/search-results-page.php" );
// JSON of catalog item for events
require_once( AC_PLUGIN_URL . "includes/json-send/json-send.php" );
// Add catalog options page
require_once( AC_PLUGIN_URL . "includes/pages/options-page.php" );
// Add custom filter to catalog listing
require_once( AC_PLUGIN_URL . "includes/admin-filter/admin-filter.php" );
// Add delete & transfer parts
require_once( AC_PLUGIN_URL . "includes/taxonomies/delete-term-transfer-products.php" );
require_once( AC_PLUGIN_URL . "includes/taxonomies/detele-term-transfer-page.php" );
require_once( AC_PLUGIN_URL . "includes/taxonomies/delete-transfer-ajax.php" );
// Importer
require_once( AC_PLUGIN_URL . "includes/products/import-csv.php" );
require_once( AC_PLUGIN_URL . "includes/pages/import-page.php" );



function ac_plugin_add_new_entities() {

    /**
    * MODEL
    */
    require_once( AC_PLUGIN_URL . 'model/Products.php' );
    require_once( AC_PLUGIN_URL . 'model/Products-tags.php' );
    require_once( AC_PLUGIN_URL . 'model/Products-categories.php' );
    require_once( AC_PLUGIN_URL . 'model/Products-brochures.php' );
    require_once( AC_PLUGIN_URL . 'model/Products-specsheets.php' );

    /**
    * Init Dependences
    */
    require_once( AC_PLUGIN_URL . 'includes/dependences/dependences-functions.php' );

}
add_action( 'init', 'ac_plugin_add_new_entities' );

/**
* Styles & Scripts
*/
function ac_add_admin_assets() {

    // Register styles for admin
    wp_register_style( 'ac-admin', plugins_url( 'assets/css/ac-admin.css', __FILE__ ) );
    wp_register_style( 'ac-admin-mediaqueries', plugins_url( 'assets/css/ac-admin-mediaqueries.css', __FILE__ ) );
    wp_register_script( 'ac-metaboxes', plugins_url( 'assets/js/ac-metaboxes.js', __FILE__ ) );
    wp_register_script( 'ac-main', plugins_url( 'assets/js/ac-main.js', __FILE__ ) );
    wp_register_script( 'ac-import', plugins_url( 'assets/js/ac-import.js', __FILE__ ) );
    wp_register_script( 'ac-tinimce', plugins_url( 'assets/js/ac-tinimce.js', __FILE__ ), array( 'mce-view' ) );

    // Enqueue styles for admin
    wp_enqueue_style( 'jquery-ui-sortable' );
    wp_enqueue_style( 'ac-admin' );
    wp_enqueue_style( 'ac-admin-mediaqueries' );

    // Enqueue scripts for admin
    wp_enqueue_script( 'underscore' );
    wp_enqueue_script( 'ac-metaboxes' );
    wp_enqueue_script( 'ac-main' );
    wp_enqueue_script( 'ac-import' );

    // Ajax URL
    wp_localize_script( 'ac-metaboxes', 'acPlugin', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );


    // Media lib scripts
    wp_enqueue_media();

    // TinyMCE
    wp_enqueue_script( 'ac-tinimce' );
}
add_action( 'admin_enqueue_scripts', 'ac_add_admin_assets' );

/************************************************************************************
* a2design addon section end
*************************************************************************************/
$one = array(
    "first" => 'one',
    "first1" => 'one1',
    "first2" => 'one2',
);
error_log( '<pre>' . serialize( $one ) . '</pre>' );
