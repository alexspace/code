<?php
// Redirect after spec sheets & brochure term deleted
function ac_redirect_after_term_deleted( $term, $tt_id, $obj ) { 
    
    if ( isset( $_GET['tag_ID'] ) ) { ?>

        <script>
            function removeURLParameter(url, parameter) {
                var urlparts= url.split('?');   
                if (urlparts.length>=2) {

                    var prefix= encodeURIComponent(parameter)+'=';
                    var pars= urlparts[1].split(/[&;]/g);

                    //reverse iteration as may be destructive
                    for (var i= pars.length; i-- > 0;) {    
                        //idiom for string.startsWith
                        if (pars[i].lastIndexOf(prefix, 0) !== -1) {  
                            pars.splice(i, 1);
                        }
                    }

                    url= urlparts[0] + (pars.length > 0 ? '?' + pars.join('&') : "");
                    return url;
                } else {
                    return url;
                }
            }
            window.location.href = '<?php echo admin_url(); ?>edit-tags.php?taxonomy=<?php echo $obj->taxonomy; ?>&post_type=ac_catalog_item';
        </script>

    <?php
    }
    
}
add_action( 'delete_ac_catalog_item_specsheets', 'ac_redirect_after_term_deleted', 10, 3 );
add_action( 'delete_ac_catalog_item_brochure', 'ac_redirect_after_term_deleted', 10, 3 );

// Add new delete type for categories
function ac_add_new_type_of_deleting( $actions, $tag ) {

    $actions['delete_transfer'] = '<a href="' . get_admin_url() . 'edit.php?post_type=ac_catalog_item&page=delete_transfer&term_id=' . $tag->term_id .'" class="" data-id="' . $tag->term_id . '">Delete & Transfer</a>';
    return $actions; 
    
}
add_filter( 'ac_category_row_actions', 'ac_add_new_type_of_deleting', 10, 2 );