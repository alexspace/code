<?php
// Sorting terms
function ac_sorting_terms() {
	
	$sort_obj = $_POST['sortObj']; 
 	
 	foreach ( $sort_obj as $key => $value ) {
 		update_term_meta( $key, 'order', $value );
 	}

	wp_die();

}
add_action( 'wp_ajax_ac_sorting_terms', 'ac_sorting_terms' );