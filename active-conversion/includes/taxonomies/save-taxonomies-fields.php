<?php

// Update file field in taxonomy
function ac_update_tax_file( $term_id ) {

    // Update Last modify datetime
    $date_modify = update_term_meta( $term_id, 'last_modify_datetime', time() );

    // Add attachmet to term if exist
    if ( isset( $_POST['ac-file-name-display'] ) ) {
        update_term_meta( $term_id, 'ac_attachment_file', $_POST['ac-file-name-display'] );
    }

    if ( isset( $_POST['ac_attachment_file'] ) ) {
        update_term_meta( $term_id, 'ac_attachment_file', $_POST['ac_attachment_file'] );
    }

    if ( isset( $_POST['taxonomy'] ) && $_POST['taxonomy'] == 'ac_category' ) {
        $create_datetime_meta = get_term_meta( $term_id, 'ac_create_datetime', true );

        if ( ! $create_datetime_meta ) {
            update_term_meta( $term_id, 'ac_create_datetime', time() );
        }

    }

    // Set default order metafield for all taxonomy types ( If it not exist terms not displayed ) 
    $order = get_term_meta( $term_id, 'order', true );

    if ( ! isset( $order ) || empty( $order ) ) {
        update_term_meta( $term_id, 'order', '0' );
    }

}
// Product brochures
add_action("create_ac_catalog_item_brochure", 'ac_update_tax_file');
add_action("edited_ac_catalog_item_brochure", 'ac_update_tax_file');
// Product specsheets
add_action("create_ac_catalog_item_specsheets", 'ac_update_tax_file');
add_action("edited_ac_catalog_item_specsheets", 'ac_update_tax_file');
// Product categories
add_action("create_ac_category", 'ac_update_tax_file');
add_action("edited_ac_category", 'ac_update_tax_file');