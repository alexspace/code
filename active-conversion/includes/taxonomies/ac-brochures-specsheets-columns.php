<?php
// Add thumbnail column to terms table for ac_category taxonomy in admin panel
function ac_modify_brochure_and_specsheets_table( $column ) {
	$product_arr = array( 'file' => 'File' );
	$new_arr = array();

	
	foreach ( $column as $key => $value ) {
		
		$new_arr[$key] = $value;
		
		if ( $key == 'name' ) {
			$new_arr = $new_arr + $product_arr;
		}

		if ( $key == 'description' ) {
			unset( $new_arr[$key] );
		}

	}

	return $new_arr;
}
add_filter( 'manage_edit-ac_catalog_item_brochure_columns', 'ac_modify_brochure_and_specsheets_table' );
add_filter( 'manage_edit-ac_catalog_item_specsheets_columns', 'ac_modify_brochure_and_specsheets_table' );

// Add thumbnail image to thumbnail column in post table in admin panel
function ac_modify_brochure_and_specsheets_table_row( $empty, $column_name, $term_id ) {

	if( $column_name == 'file' ) {
	   $file = get_term_meta( $term_id, 'ac_attachment_file', true );
	   if ( ! empty( $file ) ) {
			echo '<i class="dashicons-before dashicons-media-default"></i>' . basename( $file );
	   }

	}
         
}
add_action( 'manage_ac_catalog_item_specsheets_custom_column', 'ac_modify_brochure_and_specsheets_table_row', 10, 3 );
add_action( 'manage_ac_catalog_item_brochure_custom_column', 'ac_modify_brochure_and_specsheets_table_row', 10, 3 );