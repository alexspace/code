<?php
function ac_delete_transfer_action() {
	
	$posts = isset( $_POST['posts'] ) && ! empty( $_POST['posts'] ) ? $_POST['posts'] : array();
	$cats = isset( $_POST['cats'] ) && ! empty( $_POST['cats'] ) ? $_POST['cats'] : array();

	if ( ! empty( $posts ) && ! empty( $cats ) ) {
		
		function wpm_int( $n ) {
			return intval( $n );
		}

		$new_cats_arr = array_map( 'wpm_int', $cats );
		$new_posts_arr = array_map( 'wpm_int', $posts );

		// Trasnfer all products to selected cats
		foreach ( $new_posts_arr as $post ) {
			
			wp_set_object_terms( $post, $new_cats_arr, 'ac_category', true );

		}

		// Delete term 
		if ( isset( $_POST['deleting'] ) && ! empty( $_POST['deleting'] ) ) {
			
			wp_delete_term( intval( $_POST['deleting'] ), 'ac_category' );

		}

	}

	wp_die();
}
add_action( 'wp_ajax_ac_delete_transfer_action', 'ac_delete_transfer_action' );