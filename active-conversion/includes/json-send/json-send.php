<?php

// Create JSON for save catalog item
function ac_json_for_save_catalog_item() {

	$sku = isset( $_POST['ac-product-details']['sku'] ) && ! empty( $_POST['ac-product-details']['sku'] ) ? $_POST['ac-product-details']['sku'] : '';
	$name = isset( $_POST['post_title'] ) && ! empty( $_POST['post_title'] ) ? $_POST['post_title'] : '';
	$model = isset( $_POST['ac-product-details']['model'] ) && ! empty( $_POST['ac-product-details']['model'] ) ? $_POST['ac-product-details']['model'] : '';
	$manufacturer = isset( $_POST['ac-product-details']['manufacturer'] ) && ! empty( $_POST['ac-product-details']['manufacturer'] ) ? $_POST['ac-product-details']['manufacturer'] : '';
	$size = isset( $_POST['ac-product-details']['size'] ) && ! empty( $_POST['ac-product-details']['size'] ) ? $_POST['ac-product-details']['size'] : '';
	$price = isset( $_POST['ac-product-details']['price'] ) && ! empty( $_POST['ac-product-details']['price'] ) ? $_POST['ac-product-details']['price'] : '';
	$currency = isset( $_POST['ac-product-details']['currency'] ) && ! empty( $_POST['ac-product-details']['currency'] ) ? $_POST['ac-product-details']['currency'] : '';
	$short_description = isset( $_POST['ac-product-details']['short-description'] ) && ! empty( $_POST['ac-product-details']['short-description'] ) ? $_POST['ac-product-details']['short-description'] : '';
	$ac_category = isset( $_POST['tax_input']['ac_category'] ) && ! empty( $_POST['tax_input']['ac_category'] ) ? $_POST['tax_input']['ac_category'] : '';
	$ac_catalog_item_tags = isset( $_POST['tax_input']['ac_catalog_item_tags'] ) && ! empty( $_POST['tax_input']['ac_catalog_item_tags'] ) ? $_POST['tax_input']['ac_catalog_item_tags'] : '';
	$ac_catalog_item_specsheets = isset( $_POST['ac_catalog_item_specsheets'] ) && ! empty( $_POST['ac_catalog_item_specsheets'] ) ? $_POST['ac_catalog_item_specsheets'] : '';
	$ac_catalog_item_brochure = isset( $_POST['ac_catalog_item_brochure'] ) && ! empty( $_POST['ac_catalog_item_brochure'] ) ? $_POST['ac_catalog_item_brochure'] : '';
	
	$specs = array();
	$categories = array();
	$tags = array();
	$brochures = array();
	
	if ( ! empty( $ac_category ) ) {
		
		foreach ( $ac_category as $cat_id ) {
			
			$term = get_term( $cat_id, 'ac_category', ARRAY_A );
			if ( $term instanceof WP_Error ) continue;
			$categories[] = $term['name'];

		}

	}

	if ( ! empty( $ac_catalog_item_tags ) ) {
		
		foreach ( $ac_catalog_item_tags as $tag_id ) {
			
			$tag = get_term( $tag_id, 'ac_catalog_item_tags', ARRAY_A );
			$tags[] = $tag['name'];

		}

	}

	if ( ! empty( $ac_catalog_item_specsheets ) ) {
		
		foreach ( $ac_catalog_item_specsheets as $spec_id ) {
			
			$spec_obj = get_term( $spec_id, 'ac_catalog_item_specsheets', ARRAY_A );
			if ( $spec_obj instanceof WP_Error ) continue;
			$spec_meta = get_term_meta( $spec_id, 'ac_attachment_file', true );
			$spec_meta = isset( $spec_meta ) && ! empty( $spec_meta ) ? $spec_meta : 'file not attached';
			$specs[] = array( 
				'name' => $spec_obj['name'],
				'file_url' => $spec_meta
			);

		}

	}
	
	if ( ! empty( $ac_catalog_item_brochure ) ) {
		
		foreach ( $ac_catalog_item_brochure as $brochure_id ) {
			
			$brochure_obj = get_term( $brochure_id, 'ac_catalog_item_brochure', ARRAY_A );
			if ( $brochure_obj instanceof WP_Error ) continue;
			$brochure_meta = get_term_meta( $brochure_id, 'ac_attachment_file', true );
			$brochure_meta = isset( $brochure_meta ) && ! empty( $brochure_meta ) ? $brochure_meta : 'file not attached';
			$brochures[] = array( 
				'name' => $brochure_obj['name'],
				'file_url' => $brochure_meta
			);

		}

	}

	$php_arr = array(
		'sku' => $sku,
		'name' => $name,
		'model' => $model,
		'manufacturer' => $manufacturer,
		'size' => $size,
		'price' => $price,
		'currency' => $currency,
		'short_description' => $short_description,
		'categories' => $categories,
		'tags' => $tags,
		'specsheets' => $specs,
		'brochures' => $brochures,
	);

	$json = json_encode( $php_arr );
	// pr( json_encode( $php_arr ) );

	// exit;
}
// JSON creating when UPDATE - CREATE - DELETE product
add_action( 'save_post', 'ac_json_for_save_catalog_item' );
add_action( 'delete_post', 'ac_json_for_save_catalog_item' );