<?php
// Add options page
function ac_add_options_page_catalog() {
	add_submenu_page(
		'edit.php?post_type=ac_catalog_item',
		'Settings',
		'Settings',
		'manage_options',
		'catalog-settings',
		'ac_add_options_page_catalog_callback'
	);
}
add_action('admin_menu', 'ac_add_options_page_catalog');

function ac_add_options_page_catalog_callback() {
	$options = get_option('ac_catalog_options', '' );

	if ( isset( $_POST['ac_catalog_options'] ) ) {
		$options = $_POST['ac_catalog_options'];
		update_option( 'ac_catalog_options', $options );
	}

	?>
		<h1>Catalog settings</h1>

		<?php if ( isset( $_POST['saved'] ) && $_POST['saved'] === 'ok' ) : ?>
			<div id="message" class="updated notice notice-success is-dismissible products-options-notify">
				<p>Settings saved.</p>
				<button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button>
			</div>
		<?php endif; ?>

		<form method="POST" action="">
            <table class="form-table">
                <tr valign="top">
                    <th scope="row">
                        <label for="num_elements">
                            Product currency symbol
                        </label>
                    </th>
                    <td>
                        <input type="text" name="ac_catalog_options[currency_symbol]" value="<?php echo $options['currency_symbol']; ?>" />
                    </td>
                </tr>
                <tr>
                	<th>
						<input type="hidden" name="saved" value="ok">
                		<button class="button button-primary button-large"><?php _e( 'Save', 'ac' ); ?></button>
                	</th>
                </tr>
            </table>
        </form>
	<?php
}
