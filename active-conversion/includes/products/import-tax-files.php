<?php
require_once( ABSPATH . 'wp-admin/includes/image.php' );
require_once( ABSPATH . 'wp-admin/includes/file.php' );
include_once( ABSPATH . 'wp-admin/includes/admin.php' );

if ( ! function_exists('ac_term_file_import_functionality') ) {
	function ac_term_file_import_functionality( $files, $taxonomy ) {
		foreach ( $files as $key => $value) {
			$term = get_term_by('name', $key, $taxonomy, ARRAY_A );
			$term_meta_field = get_term_meta( $term['term_id'], 'ac_attachment_file', true );

			// Delete old file before upload new
			if ( ! empty ( $term_meta_field ) ) {
				$attach_id = get_attachment_id_from_src( $term_meta_field );
				wp_delete_attachment( $attach_id, true );
				update_term_meta( $term['term_id'], 'ac_attachment_file', '' );
			}

			$file_name = preg_replace( '/\.[^.]+$/', '', basename( $value ) );

			$file_name_with_ext = wp_basename( $value );

			$uploaddir = wp_upload_dir();
			$uploadfile = $uploaddir['path'] . '/' . $file_name_with_ext;

			$contents= file_get_contents( $value );
			$savefile = fopen($uploadfile, 'w');
			fwrite($savefile, $contents);
			fclose($savefile);
			$wp_filetype = wp_check_filetype( $file_name_with_ext, null );

			$attachment = array(
				'guid' => $uploaddir['url'] . '/' . $file_name_with_ext,
			    'post_mime_type' => $wp_filetype['type'],
			    'post_title' => $file_name_with_ext,
			    'post_content' => '',
			    'post_status' => 'inherit'
			);

			$attach_id = wp_insert_attachment( $attachment, $uploadfile );

			$imagenew = get_post( $attach_id );
			$fullsizepath = get_attached_file( $imagenew->ID );
			$attach_data = wp_generate_attachment_metadata( $attach_id, $fullsizepath );
			wp_update_attachment_metadata( $attach_id, $attach_data );
			$file_url = wp_get_attachment_url( $attach_id );
			$update = update_term_meta( $term['term_id'], 'ac_attachment_file', wp_get_attachment_url( $attach_id ) );

		}
	}
}

ac_term_file_import_functionality( $brochures, 'ac_catalog_item_brochure' );
ac_term_file_import_functionality( $spec_sheets, 'ac_catalog_item_specsheets' );