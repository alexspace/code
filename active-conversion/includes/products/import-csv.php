<?php

// Upload CSV file
function ac_import_catalog() {

	$csv_url = isset( $_POST['csv_url'] ) ? $_POST['csv_url'] : '';
	$row = 0;
	$file = fopen( $csv_url, "r" );
	$statistic = array();
	$created = 0;
	$updated = 0;
	$data = fgetcsv( $file );

	// If no user detection and no correct CSV first row structure
	if ( $data[0] != 'product' ||
		$data[1] != 'sku' ||
		$data[2] != 'tags' ||
		$data[3] != 'categories' ||
		$data[4] != 'brochures' ||
		$data[5] != 'spec sheets' ||
		$data[6] != 'model' ||
		$data[7] != 'manufacturer' ||
		$data[8] != 'size' ||
		$data[9] != 'price' ||
		$data[10] != 'currency' ||
		$data[11] != 'short description' ||
		$data[12] != 'content' ||
		$data[13] != 'excerpt' ||
		$data[14] != 'custom fields' ||
		$data[15] != 'main image' ||
		$data[16] != 'secondary images' ) {

		echo 'error';
		wp_die();

	}

	// Grap statistic
	while( ! feof( $file ) ) {
		$data = fgetcsv( $file );
		$post_exist = get_page_by_title( $data[0], OBJECT, 'ac_catalog_item');

		if ( $post_exist !== NULL ) {
			$updated++;
		} else {
			$created++;
		}

		$row++;
	}

	$statistic = array(
		'total' => $row,
		'created' => $created,
		'updated' => $updated,
	);

	fclose( $file );

	wp_send_json( $statistic );

	wp_die();

}
add_action( 'wp_ajax_ac_import_catalog', 'ac_import_catalog' );

// Confirm import after upload
function ac_confirm_import_catalog() {
	$csv_url = $_POST['csv_url'];
	$row = 0;
	$file = fopen( $csv_url, "r" );
	$data = fgetcsv( $file );
	$user = get_userdata( get_current_user_id() );

	// If no user detection and no correct CSV first row structure
	if ( ! $user ) {

		echo 'error';
		wp_die();

	}

	while( ! feof( $file ) ) {
		$data = fgetcsv( $file );
		$title = isset( $data[0] ) && ! empty( $data[0] ) ? $data[0] : '';
		$sku = isset( $data[1] ) && ! empty( $data[1] ) ? $data[1] : '';
		$tags = isset( $data[2] ) && ! empty( $data[2] ) ? $data[2] : '';
		$categories = isset( $data[3] ) && ! empty( $data[3] ) ? $data[3] : '';
		$categories = explode( ';', $categories );
		$cat_terms = array();
		$brochures = isset( $data[4] ) && ! empty( $data[4] ) ? $data[4] : '';
		$spec_sheets = isset( $data[5] ) && ! empty( $data[5] ) ? $data[5] : '';
		$model = isset( $data[6] ) && ! empty( $data[6] ) ? $data[6] : '';
		$manufacturer = isset( $data[7] ) && ! empty( $data[7] ) ? $data[7] : '';
		$size = isset( $data[8] ) && ! empty( $data[8] ) ? $data[8] : '';
		$price = isset( $data[9] ) && ! empty( $data[9] ) ? $data[9] : '';
		$currency = isset( $data[10] ) && ! empty( $data[10] ) ? $data[10] : '';
		$short_description = isset( $data[11] ) && ! empty( $data[11] ) ? $data[11] : '';
		$content = isset( $data[12] ) && ! empty( $data[12] ) ? $data[12] : '';
		$excerpt = isset( $data[13] ) && ! empty( $data[13] ) ? $data[13] : '';
		$custom_meta_fields = isset( $data[14] ) && ! empty( $data[14] ) ? $data[14] : '';

		$main_image = isset( $data[15] ) && ! empty( $data[15] ) ? $data[15] : '';
		$secondary_images = isset( $data[16] ) && ! empty( $data[16] ) ? $data[16] : '';
		$secondary_images = explode( ',', $secondary_images);

		$id = get_page_by_title( $title, OBJECT, 'ac_catalog_item' );
		$id = ($id != NULL) ? $id->ID : '';

		// Parse brochures
		$brochures = ac_parce_complex_cvs( $brochures );
		$brochures_names_arr = array();
		foreach ( $brochures as $key => $value) {
			$brochures_names_arr[] = $key;
		}

		// Parse spec sheets
		$spec_sheets = ac_parce_complex_cvs( $spec_sheets );
		$spec_sheets_names_arr = array();
		foreach ( $spec_sheets as $key => $value) {
			$spec_sheets_names_arr[] = $key;
		}

		// Parse categories
		foreach ( $categories as $csv_field ) {
		    $children_terms_pattern = '/\(.+\)/';
		    $main_term_pattern = '/^[^(]*/';

		    preg_match( $main_term_pattern , $csv_field, $main_term_matches );
		    preg_match_all( $children_terms_pattern, $csv_field, $children_matches );

			// Create main term if not exits
		    $term = term_exists( $main_term_matches[0], 'ac_category' );
		    if ( ! is_array( $term ) || $term === 0 || $term === null ) {
		        $term = wp_insert_term( $main_term_matches[0], 'ac_category' );
		    } elseif ( is_array( $term ) ) {
		    	$term_id = $term['term_id'];
		    	array_push( $cat_terms, intval( $term_id ) );
			}

		    // Create children term if not exits
		    if ( ! empty( array_filter( $children_matches ) ) ) {
		        if ( $children_matches[0][0] ) {
					$childrens_arr = explode( ',', trim( $children_matches[0][0], '()' ) );
					foreach ( $childrens_arr as $child_term ) {
						$child_post = wp_insert_term( $child_term, 'ac_category', array( 'parent' => $term['term_id'] ) );
						if ( ! $child_post instanceof WP_Error ) {
							array_push( $cat_terms, intval( $child_post['term_id'] ) );
						}
					}
				}

		    }

		}

		// Parse custom meta
		$custom_meta_fields = ac_parce_complex_cvs( $custom_meta_fields );
		$custom_meta_fields_arr = array();
		foreach ( $custom_meta_fields as $key => $value ) {
		    $key = strtolower( str_replace( ' ', '_', $key ) );
		    $custom_meta_fields_arr[$key] = $value;
		}

		// Args for import
		$args = array(
			'ID' => $id,
			'post_title' => $title,
			'post_author' => $user->user_login,
			'post_content' => $content,
			'post_excerpt' => $excerpt,
			'post_status' => 'publish',
			'post_type' => 'ac_catalog_item',
			'tax_input' => array(
				'ac_catalog_item_tags' => $tags,
				'ac_catalog_item_brochure' => $brochures_names_arr,
				'ac_catalog_item_specsheets' => $spec_sheets_names_arr,
			),
			'meta_input' => array(
				'ac-product-details' => array(
					'sku' => $sku,
					'model' => $model,
					'manufacturer' => $manufacturer,
					'size' => $size,
					'price' => $price,
					'currency' => $currency,
					'short-description' => $short_description
				)
			)

		);

		// Add custom meta data to args
		if ( ! empty( $custom_meta_fields_arr ) ) {
			foreach ( $custom_meta_fields_arr as $key => $value ) {
				$args['meta_input'][$key] = $value;
			}
		}

		$post = wp_insert_post( $args );
		wp_set_object_terms( $post, $cat_terms, 'ac_category' );

		// Files import functionality
		require( AC_PLUGIN_URL . 'includes/products/import-tax-files.php' );
		// Image import functionalit
		$post_main_image = get_post_meta( $post, '_ac_main_product_image', true );
		$post_additional_images = get_post_meta( $post, '_ac_additional_product_images', true );
		if ( ! empty( $post_main_image ) ) {

		    $post_main_image = json_decode( $post_main_image );

		    foreach ( $post_main_image as $key => $value ) {
		    	wp_delete_attachment( $value->image_id, true );
		    }

		    update_post_meta( $post, '_ac_main_product_image', '' );

		}

		if ( ! empty( $post_additional_images ) ) {

		    $post_additional_images = json_decode( $post_additional_images );

		    foreach ( $post_additional_images as $key => $value ) {
		    	wp_delete_attachment( $value->image_id, true );
		    }

		    update_post_meta( $post, '_ac_additional_product_images', '' );

		}

		require( AC_PLUGIN_URL . 'includes/products/import-main-image.php' );
		// Secondary images functionality
		require( AC_PLUGIN_URL . 'includes/products/import-secondary-images.php' );

		$row++;
	}

	fclose( $file );

	wp_die();
}
add_action( 'wp_ajax_ac_confirm_import_catalog', 'ac_confirm_import_catalog' );
