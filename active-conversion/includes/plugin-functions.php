<?php

/*
 * Change the featured image metabox link text
 */
function ac_change_featured_image_text( $content ) {

	if ( 'ac_catalog_item' === get_post_type() ) {
		$content = str_replace( 'Set featured image', __( 'Set product image', 'ac' ), $content );
		$content = str_replace( 'Remove featured image', __( 'Remove product image', 'ac' ), $content );
	}
	return $content;

}
add_filter( 'admin_post_thumbnail_html', 'ac_change_featured_image_text' );

// Change button text in Media uploader
function wpse_41767_change_image_button( $translation, $text, $domain ) {

    if ( $text == 'Insert into post' ) {
        // Once is enough.
        remove_filter( 'gettext', 'wpse_41767_change_image_button' );
        return 'Use this';
    }
    return $translation;

}
add_filter( 'gettext', 'wpse_41767_change_image_button', 10, 3 );

// Change default term order
function ac_change_default_terms_order( $defaults, $taxonomies ) {

    if ( in_array( 'ac_catalog_item_specsheets', $taxonomies ) ||
        in_array( 'ac_catalog_item_brochure', $taxonomies ) ||
        in_array( 'ac_category', $taxonomies ) ) {

        $custom = array(
            'meta_key' => 'order',
            'orderby' => 'meta_value_num',
        );
        $new_arr = array_replace( $defaults, $custom );
        return $new_arr;

    } else {

        return $defaults;

    }

}
add_filter( 'get_terms_args', 'ac_change_default_terms_order', 10, 2 );

// Show all hooks on pages
// $debug_tags = array();
//add_action( 'all', function ( $tag ) {
// global $debug_tags;
// if ( in_array( $tag, $debug_tags ) ) {
//     return;
// }
// echo "<pre>" . $tag . "</pre>";
// $debug_tags[] = $tag;
//} );

function ac_add_import_button( $default ) {

    if ( isset( $_GET['post_type'] ) && $_GET['post_type'] === 'ac_catalog_item' ) {

        echo '
            <div class="import-btn-container">
                <a href="" class="button button-primary button-large ac-add-catalog">Import catalog</a><span class="spinner"></span>
            </div>
            <div class="ac-import-popup">
                <div class="import-popup-inner">
                    <i class="dashicons dashicons-no-alt"></i>
                    <div class="import-infobox">

                    </div>
                    <div class="button-box">
						<p class="warning-popup-message">Don\'t close this popup before import procedure will be ended!</p>
                        <span class="spinner"></span>
                        <button class="button button-primary button-large js-confirm-import">Confirm import</button>
                        <button class="button button-primary button-large js-cancel-import">Cancel</button>
                    </div>
                </div>
            </div>
        ';

    }
    return $default;

}
add_filter('views_edit-ac_catalog_item', 'ac_add_import_button', 10 );

// Add search ability to custom columns
function ac_search_meta_products_in_lists( $query ) {

    if ( is_admin() && ! empty( $query->get('s') ) ) {

        $variable_to_send = isset( $_GET['s'] ) && ! empty( $_GET['s'] ) ? $_GET['s'] : '';
        wp_redirect( get_bloginfo( 'url' ) . '/wp-admin/options.php?page=ac-search&s=' . $variable_to_send );
        exit;
    }
}
add_action( 'pre_get_posts', 'ac_search_meta_products_in_lists' );

// Parse complex CSV fields for import procedure
function ac_parce_complex_cvs( $field ) {

    preg_match_all( '/[^,]*[=>][^,]*/', $field, $matches );
    $field_arr = array();
    $brochures_names = array();
    foreach ( $matches[0] as $el ) {
        preg_match( '/^[^=>]*/', trim( $el ), $el_key );
        preg_match( '/[^=>]*$/', trim( $el ), $el_value );

        if ( isset( $el_key[0] ) && ! empty( $el_key[0] ) ) {
            $field_arr[trim($el_key[0])] = isset( $el_value[0] ) ? trim( $el_value[0] ) : '';
        }

    }
    return $field_arr;

}

function get_attachment_id_from_src($image_src) {

    global $wpdb;
    $query = "SELECT ID FROM {$wpdb->posts} WHERE guid='$image_src'";
    $id = $wpdb->get_var($query);
    return ! empty( $id ) ? $id : false;

}

function get_post_modify_date_by_id( $id ) {

    global $wpdb;
    $query = "SELECT post_modified FROM {$wpdb->posts} WHERE ID='$id'";
    $modify_date = $wpdb->get_var($query);
    return ! empty( $modify_date ) ? $modify_date : false;

}

function myUrlEncode($string) {
    $entities = array('%21', '%2A', '%27', '%28', '%29', '%3B', '%3A', '%40', '%26', '%3D', '%2B', '%24', '%2C', '%2F', '%3F', '%25', '%23', '%5B', '%5D');
    $replacements = array('!', '*', "'", "(", ")", ";", ":", "@", "&", "=", "+", "$", ",", "/", "?", "%", "#", "[", "]");
    return str_replace($entities, $replacements, urlencode($string));
}

// Change dinamically secondary images order if order 0 for all
function ac_recursive_cheking( $image_order, $arr ) {
	if ( array_key_exists( $image_order, $arr ) ) {
		$image_order += 1;
		return ac_recursive_cheking( $image_order, $arr );
	} else {
		return $image_order;
	}
}

function create_new_url_querystring() {

    add_rewrite_tag('%sku%','([^/]*)');
    add_rewrite_tag('%model%','([^/]*)');

    add_rewrite_rule(
        '^products/([^/]*)_([^/]*)_([^/]*)$',
        'index.php?sku=$matches[1]&model=$matches[2]&products=$matches[3]',
        'top'
    );
    add_rewrite_rule(
        '^products/([^/]*)$',
        'index.php?products=$matches[1]',
        'top'
    );
    flush_rewrite_rules();

    global $wp_query;

}
add_action('init', 'create_new_url_querystring');

function ac_add_query_vars( $id ) {
    global $wp_query;
    $ac_product_details = get_post_meta( $id, 'ac-product-details', true );

    if ( isset( $ac_product_details['sku'] ) && ! empty( $ac_product_details['sku'] ) ) {
        $sku = $ac_product_details['sku'];
        set_query_var( 'sku', $sku );
    }

    if ( isset( $ac_product_details['model'] ) && ! empty( $ac_product_details['model'] ) ) {
        $model = $ac_product_details['model'];
        set_query_var( 'model', $model );
    }

}
add_action( 'save_post', 'ac_add_query_vars');

function ac_get_perm_html( $permalink, $post, $leavename ) {

    $ac_product_details = get_post_meta( $post->ID, 'ac-product-details', true );

    if ( isset( $ac_product_details['sku'] ) && ! empty( $ac_product_details['sku'] ) ) {
        $sku = $ac_product_details['sku'];
    } else {
        $sku = false;
    }

    if ( isset( $ac_product_details['model'] ) && ! empty( $ac_product_details['model'] ) ) {
        $model = $ac_product_details['model'];
    } else {
        $model = false;
    }

    $permalink = str_replace( '%sku%', $sku, $permalink );
    $permalink = str_replace( '%model%', $model, $permalink );

    if ( ! $sku || ! $model ) {
        return site_url('products/' . $post->post_name );
    }
    return site_url('products/' . $sku . '_' . $model . '_' . $post->post_name );
}
add_filter( 'post_type_link', 'ac_get_perm_html', 10, 3 );