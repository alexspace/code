<?php
// Post details metabox
function ac_productdetails() { 
	global $post;
	$ac_product_details = get_post_meta( $post->ID, 'ac-product-details', true );
	?>
	<div class="product-details-metabox-container">
		<table>

			<tr>
				<th>
					SKU
				</th>
				<td>
					<input type="text" name="ac-product-details[sku]" value="<?php echo isset( $ac_product_details['sku'] ) ? $ac_product_details['sku'] : ''; ?>">
				</td>

				
			</tr>

			<tr>
				<th>
					Model
				</th>
				<td>
					<input type="text" name="ac-product-details[model]" value="<?php echo isset( $ac_product_details['model']) ? $ac_product_details['model'] : ''; ?>">
				</td>
			</tr>

			<tr>
				<th>
					Manufacturer
				</th>
				<td>
					<input type="text" name="ac-product-details[manufacturer]" value="<?php echo isset( $ac_product_details['manufacturer'] ) ? $ac_product_details['manufacturer'] : ''; ?>">
				</td>
			</tr>

			<tr>
				<th>
					Size
				</th>
				<td>
					<input type="text" name="ac-product-details[size]" value="<?php echo isset( $ac_product_details['size'] ) ? $ac_product_details['size'] : ''; ?>">
				</td>
			</tr>

			<tr>
				<th>
					Price
				</th>
				<td>
					<input type="text" name="ac-product-details[price]" value="<?php echo isset( $ac_product_details['price'] ) ? $ac_product_details['price'] : ''; ?>">
				</td>
			</tr>

			<tr>
				<th>
					Currency
				</th>
				<td>
					<input type="text" name="ac-product-details[currency]" value="<?php echo isset( $ac_product_details['currency'] ) ? $ac_product_details['currency'] : ''; ?>">
				</td>
			</tr>

			<tr>
				<th>
					Short description
				</th>
				<td>
					<input type="text" name="ac-product-details[short-description]" value="<?php echo isset( $ac_product_details['short-description'] ) ? $ac_product_details['short-description'] : ''; ?>">
				</td>
			</tr>

		</table>

	</div>

	<?php
}