<?php
/**
* After we checked some terms we need them associate with post after post "Update"
*/
function ac_associate_terms_after_post_updated( $post_id ) {

	// Specsheets
	if ( isset( $_POST['ac_catalog_item_specsheets'] ) ) {
		$terms_arr = $_POST['ac_catalog_item_specsheets'];

		foreach ( $terms_arr as $value ) {

			wp_set_post_terms( $_POST['post_ID'], $value, 'ac_catalog_item_specsheets', true );

		}

	}

	// Brochures
	if ( isset( $_POST['ac_catalog_item_brochure'] ) ) {
		$terms_arr = $_POST['ac_catalog_item_brochure'];

		foreach ( $terms_arr as $value ) {

			wp_set_post_terms( $_POST['post_ID'], $value, 'ac_catalog_item_brochure', true );

		}

	}

	// Main Image
	if ( isset( $_POST['ac-main-product-image'] ) ) {

		$image_main_json = str_replace( '\\', '', $_POST['ac-main-product-image'] );
		$image_main_json = str_replace( "'", '"', $image_main_json );
		update_post_meta( $post_id, '_ac_main_product_image', $image_main_json );

	}

	// Additional Images
	if ( isset( $_POST['ac-additional-product-images'] ) ) {

		$images_additional_json = str_replace( '\\', '', $_POST['ac-additional-product-images'] );
		$images_additional_json = str_replace( "'", '"', $images_additional_json );
		update_post_meta( $post_id, '_ac_additional_product_images', $images_additional_json );

	}

	// Product details
	if ( isset( $_POST['ac-product-details'] ) ) {
		update_post_meta( $post_id, 'ac-product-details', $_POST['ac-product-details'] );
	}

}
add_action( 'save_post', 'ac_associate_terms_after_post_updated', 10 );
