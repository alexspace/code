<?php

// Callback for specsheets metaboxe
function ac_catalog_item_specsheets_meta_box() {
	ac_taxonomy_metabox_view( 'ac_catalog_item_specsheets' );
}

// Callback for brochures metaboxe
function ac_catalog_item_brochure_meta_box() {
	ac_taxonomy_metabox_view( 'ac_catalog_item_brochure' );
}

// Metabox view
function ac_taxonomy_metabox_view( $tax_name ) {
	global $post;
	$taxonomy = get_taxonomy( $tax_name );
	$terms = get_terms( $tax_name, array(
	    'hide_empty' => false,
	    'meta_key' => 'order',
	    'orderby' => 'meta_value_num',
	    'order' => 'ASC'
	) ); ?>
		
	<div class="ac-show-all-term-list">
		<div class="ac-preloader-checklist">
			<span class="ac-spinner ac-spinner-metabox spinner is-active"></span>
		</div>

		<?php if ( ! empty( $terms ) ) : ?>
			<ul>
				<?php foreach ( $terms as $value) : ?>
					<?php 
						$file = get_term_meta( $value->term_id, 'ac_attachment_file' );
						if ( isset( $file ) && ! empty( $file ) ) {
							$file = $file[0];
							$pattern = '/(\w+)(\.\w+)+(?!.*(\w+)(\.\w+)+)/';
							preg_match( $pattern, $file, $matches );
						}
						$file_name = isset( $matches[0] ) && ! empty( $matches[0] ) ? $matches[0] : '';
					?>
					<li class="sortable-list-item">
						<label class="selectit">
							
							<input type="checkbox" name="<?php echo $tax_name . '[term' . $value->term_id . ']'; ?>" value="<?php echo $value->term_id; ?>" <?php echo has_term( $value->term_id, $tax_name, $post ) ? 'checked="checked"' : ''; ?>>
							<?php echo $value->name; ?>

							<?php if ( $file_name ) : ?>
								<span class="ac-metabox-file-name-preview">"<?php echo $file_name; ?>"</span>
							<?php endif; ?>
							
						</label>
					</li>
				<?php endforeach; ?>
			</ul>
		<?php else : ?>
			<ul>
				<li class="ac-no-terms-in-list">Add some <?php echo $taxonomy->labels->name; ?>!</li>
			</ul>
		<?php endif; ?>

	</div>

	
	<div class="ac-inner-metabox-field">
		<label class="ac-metabox-label">Add new <?php echo $taxonomy->labels->singular_name; ?></label>
		<span class="ac-spinner ac-spinner-metabox spinner is-active"></span>
		<span class="ac-uploaded-value-path"></span>

		<input class="ac-metabox-text-field" type="text" name="<?php echo $tax_name; ?>-title" placeholder="Name...">
		<input class="js-not-visible" type="file" name="<?php echo $tax_name; ?>-file" value="">
		
		<a href="#" class="ac-button-admin-icon button button-primary button-large js-ac-metabox-btn">
			<div class="dashicons-before dashicons-media-default"><br></div>
			Upload file
		</a>

		<button class="ac-metabox-btn js-metabox-add-button button button-primary button-large">Add new</button>

		<p class="ac-message-metabox"></p>
		<input type="hidden" class="uploaded_file_url" name="file_url">
	</div>

	<?php
	wp_nonce_field( 'ac_add_new_taxonomy_from_metaboxes', '_ajax_nonce' );
}