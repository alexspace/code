<?php

// Add new taxonomy section in metabox
function ac_add_new_taxonomy_from_metaboxes() {

	// Add new term for taxonomy in $_POST['taxonomy']
	$new_term = wp_insert_term( $_POST['name'], $_POST['taxonomy'] );

	if ( ! is_array( $new_term ) && get_class( $new_term ) == 'WP_Error' ) {
		echo 'This name already exists.';
	} else {
		echo '<div class="dashicons-before dashicons-yes"><br></div>Added';
		$meta = add_term_meta( $new_term['term_id'], 'ac_attachment_file', $_POST['file_url'] );
	}

	wp_die();

}
add_action( 'wp_ajax_ac_add_new_taxonomy_from_metaboxes', 'ac_add_new_taxonomy_from_metaboxes' );

/**
* After we added the taxonomy term though ajax in post editing
* we should update term list above adding section
*/
function ac_update_term_list_after_add_new() {
	$terms = get_terms( $_POST['taxonomy'], array(
	    'hide_empty' => false,
	) );
	$post = $_POST['post_id'];

	if ( ! empty( $terms ) ) : ?>

		<ul>
			<div class="ac-preloader-checklist">
				<span class="ac-spinner ac-spinner-metabox spinner is-active"></span>
			</div>
			<?php foreach ( $terms as $value ) : ?>
				<?php
					$file = get_term_meta( $value->term_id, 'ac_attachment_file' );
					if ( isset( $file ) && ! empty( $file ) ) {
						$file = $file[0];
						$pattern = '/(\w+)(\.\w+)+(?!.*(\w+)(\.\w+)+)/';
						preg_match( $pattern, $file, $matches );
					}
					$file_name = isset( $matches[0] ) && ! empty( $matches[0] ) ? $matches[0] : '';
				?>
				<li>
					<label class="selectit">
						<input type="checkbox" name="<?php echo $_POST['taxonomy'] . '[term' . $value->term_id . ']'; ?>" value="<?php echo $value->term_id; ?>" <?php echo has_term( $value->term_id, $_POST['taxonomy'], $post ) ? 'checked="checked"' : ''; ?>>
						<?php echo $value->name; ?>

						<?php if ( $file_name ) : ?>
							<span class="ac-metabox-file-name-preview">"<?php echo $file_name; ?>"</span>
						<?php endif; ?>
					</label>
				</li>
			<?php endforeach; ?>
		</ul>

	<?php endif;
	wp_die();
}
add_action( 'wp_ajax_ac_update_term_list_after_add_new', 'ac_update_term_list_after_add_new' );
