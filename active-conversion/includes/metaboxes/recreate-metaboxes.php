<?php
/*
 * Change the featured image metabox title text
 */
function ac_recreate_metaboxes() {

	$specsheets_icon = '<i class="ac-metabox-title-icon dashicons-before dashicons-editor-table"></i>'; 
	$brochures_icon = '<i class="ac-metabox-title-icon dashicons-before dashicons-media-document"></i>'; 
	$image_icon = '<i class="ac-metabox-title-icon dashicons-before dashicons-format-image"></i>'; 
	$tags_icon = '<i class="ac-metabox-title-icon dashicons-before dashicons-admin-post"></i>'; 
	$categories_icon = '<i class="ac-metabox-title-icon dashicons-before dashicons-networking"></i>'; 
	$cart_icon = '<i class="ac-metabox-title-icon dashicons-before dashicons-cart"></i>'; 

	// Metabox 'Product Specsheets'
	remove_meta_box( 'tagsdiv-ac_catalog_item_specsheets', 'ac_catalog_item', 'side' );
	add_meta_box( 'ac_catalog_item_specsheetsdiv', $specsheets_icon . __( 'Product spec sheets', 'ac' ), 'ac_catalog_item_specsheets_meta_box', 'ac_catalog_item', 'side' );

	// Metabox 'Product Brochures'
	remove_meta_box( 'tagsdiv-ac_catalog_item_brochure', 'ac_catalog_item', 'side' );
	add_meta_box( 'ac_catalog_item_brochurediv', $brochures_icon . __( 'Product brochures', 'ac' ), 'ac_catalog_item_brochure_meta_box', 'ac_catalog_item', 'side' );
	
	// Metabox 'Product Image'
	remove_meta_box( 'postimagediv', 'ac_catalog_item', 'side' );
	add_meta_box( 'postimagediv', $image_icon . __( 'Product images', 'ac' ), 'ac_product_images_meta_box', 'ac_catalog_item', 'side' );

	// Metabox 'Product Category'
	remove_meta_box( 'ac_categorydiv', 'ac_catalog_item', 'side' );
	add_meta_box( 'ac_categorydiv', $categories_icon . __( 'Product categories', 'ac' ), 'post_categories_meta_box', 'ac_catalog_item', 'side', 'default', array( 'taxonomy' => 'ac_category' ) );

	// Metabox 'Product Tags'
	remove_meta_box( 'tagsdiv-ac_catalog_item_tags', 'ac_catalog_item', 'side' );
	add_meta_box( 'tagsdiv-ac_catalog_item_tags', $tags_icon . __( 'Product tags', 'ac' ), 'post_tags_meta_box', 'ac_catalog_item', 'side', 'default', array( 'taxonomy' => 'ac_catalog_item_tags' ) );

	// Metabox 'Product Options'
	add_meta_box( 'product-detailsdiv', $cart_icon . __( 'Product details', 'ac' ), 'ac_productdetails', 'ac_catalog_item', 'normal', 'high' );

}
add_action('do_meta_boxes', 'ac_recreate_metaboxes', 10 );