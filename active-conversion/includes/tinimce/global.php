<?php
// Check permissions and add hooks
function ac_add_tc_shortcodes_button() { 
	global $typenow; 

	// check user permissions 
	if ( !current_user_can('edit_posts') && !current_user_can('edit_pages') ) { 
		return; 
	}

	// verify the post type 
	if( ! in_array( $typenow, array( 'ac_catalog_item', 'page' ) ) ) {
		return;
	}  

	// check if WYSIWYG is enabled 
	if ( get_user_option('rich_editing') == 'true') { 
		
		add_filter("mce_external_plugins", "ac_add_tinymce_plugin"); 
		add_filter('mce_buttons', 'ac_register_my_tc_button'); 

	}


}
add_action( 'admin_head', 'ac_add_tc_shortcodes_button' );

function ac_add_tinymce_plugin( $plugin_array ) { 
	
	// CHANGE THE BUTTON SCRIPT HERE return $plugin_array; 
	$plugin_array['ac_shortcodes'] = plugins_url( 'assets/js/ac-tinimce.js', dirname( dirname(__FILE__) ) );
	return $plugin_array;

}

function ac_register_my_tc_button( $buttons ) { 
	
	array_push( $buttons, "ac_shortcodes" ); 
	return $buttons; 

}