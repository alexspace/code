<?php

/**
 * Register a product post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */

$labels = array(
     'name'               => __( 'Product', 'ac' ),
     'singular_name'      => __( 'Product', 'ac' ),
     'menu_name'          => __( 'Products', 'ac' ),
     'name_admin_bar'     => __( 'Product', 'ac' ),
     'add_new'            => __( 'Add New Product', 'ac' ),
     'add_new_item'       => __( 'Add New Product', 'ac' ),
     'new_item'           => __( 'New Product', 'ac' ),
     'edit_item'          => __( 'Edit Product', 'ac' ),
     'view_item'          => __( 'View Product', 'ac' ),
     'all_items'          => __( 'Products', 'ac' ),
     'search_items'       => __( 'Search Products', 'ac' ),
     'parent_item_colon'  => __( 'Parent Products:', 'ac' ),
     'not_found'          => __( 'No Products found.', 'ac' ),
     'not_found_in_trash' => __( 'No Products found in Trash.', 'ac' )
);

$args = array(
     'labels'             => $labels,
     'description'        => __( 'Products', 'ac' ),
     'public'             => true,
     'publicly_queryable' => true,
     'show_ui'            => true,
     'show_in_menu'       => true,
     'query_var'          => 'products',
     'rewrite'            => false,
     'capability_type'    => 'post',
     'has_archive'        => true,
     'hierarchical'       => false,
     'menu_position'      => null,
     'menu_icon'          => plugins_url( 'assets/images/favicon.png', dirname( __FILE__ ) ),
     'supports'           => array( 'title', 'custom-fields', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
);

register_post_type( 'ac_catalog_item', $args );
