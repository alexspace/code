<?php
// Add new taxonomy, NOT hierarchical
$labels = array(
	'name'                       => _x( 'Product tags', 'taxonomy general name' ),
	'singular_name'              => _x( 'Product tag', 'taxonomy singular name' ),
	'search_items'               => __( 'Search Product tag' ),
	'popular_items'              => __( 'Popular Product tags' ),
	'all_items'                  => __( 'All Product tags' ),
	'parent_item'                => null,
	'parent_item_colon'          => null,
	'edit_item'                  => __( 'Edit Product tag' ),
	'update_item'                => __( 'Update Product tag' ),
	'add_new_item'               => __( 'Add New Product tag' ),
	'new_item_name'              => __( 'New Product tag Name' ),
	'separate_items_with_commas' => __( 'Separate Product tags with commas' ),
	'add_or_remove_items'        => __( 'Add or remove Product tags' ),
	'choose_from_most_used'      => __( 'Choose from the most used Product tags' ),
	'not_found'                  => __( 'No Product tags found.' ),
	'menu_name'                  => __( 'Tags' ),
);

$args = array(
	'hierarchical'          => false,
	'labels'                => $labels,
	'show_ui'               => true,
	'show_admin_column'     => true,
	'update_count_callback' => '_update_post_term_count',
	'query_var'             => true,
	'rewrite'               => array( 'slug' => 'ac-catalog-item-tags' ),
);

register_taxonomy( 'ac_catalog_item_tags', 'ac_catalog_item', $args );
