<?php
// Add new taxonomy, make it hierarchical (like categories)
$labels = array(
	'name'              => _x( 'Product categories', 'taxonomy general name' ),
	'singular_name'     => _x( 'Product category', 'taxonomy singular name' ),
	'search_items'      => __( 'Search Product categories' ),
	'all_items'         => __( 'All Product categories' ),
	'parent_item'       => __( 'Parent Product category' ),
	'parent_item_colon' => __( 'Parent Product category:' ),
	'edit_item'         => __( 'Edit Product category' ),
	'update_item'       => __( 'Update Product category' ),
	'add_new_item'      => __( 'Add New Product category' ),
	'new_item_name'     => __( 'New Product category Name' ),
	'menu_name'         => __( 'Categories' ),
);

$args = array(
	'hierarchical'      => true,
	'labels'            => $labels,
	'show_ui'           => true,
	'show_admin_column' => true,
	'query_var'         => true,
	'rewrite'           => array( 'slug' => 'ac-category' ),
);

register_taxonomy( 'ac_category', array( 'ac_catalog_item' ), $args );
