<?php
// Add new taxonomy, make it hierarchical (like categories)
$labels = array(
	'name'              => _x( 'Product brochures', 'taxonomy general name' ),
	'singular_name'     => _x( 'Product brochure', 'taxonomy singular name' ),
	'search_items'      => __( 'Search Product brochures' ),
	'all_items'         => __( 'All Product brochures' ),
	'popular_items'     => __( 'Popular brochures' ),
	'parent_item'       => __( 'Parent Product brochure' ),
	'parent_item_colon' => __( 'Parent Product brochure:' ),
	'edit_item'         => __( 'Edit Product brochure' ),
	'update_item'       => __( 'Update Product brochure' ),
	'add_new_item'      => __( 'Add New Product brochure' ),
	'new_item_name'     => __( 'New Product brochure Name' ),
	'menu_name'         => __( 'Brochures' ),
	'separate_items_with_commas' => __( 'Separate Product brochures with commas' ),
	'choose_from_most_used'      => __( 'Choose from the most used Product brochures' ),
);

$args = array(
	'hierarchical'      => false,
	'labels'            => $labels,
	'show_ui'           => true,
	'show_admin_column' => true,
	'query_var'         => true,
	'rewrite'           => array( 'slug' => 'ac-catalog-item-brochures' ),
);

register_taxonomy( 'ac_catalog_item_brochure', array( 'ac_catalog_item' ), $args );
