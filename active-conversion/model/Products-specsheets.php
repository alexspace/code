<?php
// Add new taxonomy, make it hierarchical (like categories)
$labels = array(
	'name'              => _x( 'Product spec sheets', 'taxonomy general name' ),
	'singular_name'     => _x( 'Product spec sheets', 'taxonomy singular name' ),
	'search_items'      => __( 'Search Product spec sheets' ),
	'all_items'         => __( 'All Product spec sheets' ),
	'popular_items'     => __( 'Popular spec sheets' ),
	'parent_item'       => __( 'Parent Product spec sheets' ),
	'parent_item_colon' => __( 'Parent Product spec sheets:' ),
	'edit_item'         => __( 'Edit Product spec sheets' ),
	'update_item'       => __( 'Update Product spec sheets' ),
	'add_new_item'      => __( 'Add New Product spec sheets' ),
	'new_item_name'     => __( 'New Product spec sheets Name' ),
	'menu_name'         => __( 'Spec sheets' ),
);

$args = array(
	'hierarchical'      => false,
	'labels'            => $labels,
	'show_ui'           => true,
	'show_admin_column' => true,
	'query_var'         => true,
	'rewrite'           => array( 'slug' => 'ac-catalog-item-specsheets' ),
);

register_taxonomy( 'ac_catalog_item_specsheets', array( 'ac_catalog_item' ), $args );
