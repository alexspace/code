jQuery(document).ready(function($) {

	// Import popup
	if ( typeof tinymce != "undefined" ) {
		tinymce.PluginManager.add('ac_shortcodes', function( editor, url ) {

			editor.addButton( 'ac_shortcodes', {
				text: '[SKU]',
				icon: 'icon ac-shortcodes-icon',
				type: 'menubutton',
				onclick: function(e) {
					e.stopPropagation();
					editor.insertContent(this.value());
				},
				menu: [
					{
						text: 'SKU',
						value: '[ac_item type="sku" id="{Please enter a SKU}"]',
						onclick: function() {
							editor.insertContent(this.value());
						}
					},
					{
						text: 'Name',
						value: '[ac_item type="name" id="{Please enter a SKU}"]',
						onclick: function() {
							editor.insertContent(this.value());
						},
					},
					{
						text: 'Model',
						value: '[ac_item type="model" id="{Please enter a SKU}"]',
						onclick: function() {
							editor.insertContent(this.value());
						},
					},
					{
						text: 'Manufacturer',
						value: '[ac_item type="mfg" id="{Please enter a SKU}"]',
						onclick: function() {
							editor.insertContent(this.value());
						},
					},
					{
						text: 'Size',

						onclick: function() {
							editor.insertContent(this.value());
						},
						menu: [
							{
								text: 'Original',
								value: '[ac_item type="size_origin" id="{Please enter a SKU}"]',
								onclick: function() {
									editor.insertContent(this.value());
								}
							},
							{
								text: 'Thumbnail',
								value: '[ac_item type="size_thumbnail" id="{Please enter a SKU}"]',
								onclick: function() {
									editor.insertContent(this.value());
								}
							},
							{
								text: 'Medium',
								value: '[ac_item type="size_medium" id="{Please enter a SKU}"]',
								onclick: function() {
									editor.insertContent(this.value());
								}
							},
							{
								text: 'Large',
								value: '[ac_item type="size_large" id="{Please enter a SKU}"]',
								onclick: function() {
									editor.insertContent(this.value());
								}
							}
						]
					},
					{
						text: 'Price',
						value: '[ac_item type="price" id="{Please enter a SKU}"]',
						onclick: function() {
							editor.insertContent(this.value());
						},
					},
					{
						text: 'Unit',
						value: '[ac_item type="unit" id="{Please enter a SKU}"]',
						onclick: function() {
							editor.insertContent(this.value());
						},
					},
					{
						text: 'Currency',
						value: '[ac_item type="currency" id="{Please enter a SKU}"]',
						onclick: function() {
							editor.insertContent(this.value());
						},
					},
					{
						text: 'Short description',
						value: '[ac_item type="sdesc" id="{Please enter a SKU}"]',
						onclick: function() {
							editor.insertContent(this.value());
						},
					},
					{
						text: 'Long description',
						value: '[ac_item type="ldesc" id="{Please enter a SKU}"]',
						onclick: function() {
							editor.insertContent(this.value());
						},
					},
					{
						text: 'Picture',
						value: '[ac_item type="pic" id="{Please enter a SKU}"]',
						onclick: function() {
							editor.insertContent(this.value());
						},
					},
					{
						text: 'Brochures',
						value: '[ac_item type="brochures" id="{Please enter a SKU}"]',
						onclick: function() {
							editor.insertContent(this.value());
						},
					},
					{
						text: 'Specsheets',
						value: '[ac_item type="specsheets" id="{Please enter a SKU}"]',
						onclick: function() {
							editor.insertContent(this.value());
						},
					}
				]
			});

		});
	}

});
