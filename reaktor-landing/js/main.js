jQuery(document).ready(function($) {
	/**
	 * Header Menu
	 */
	if ( $('.b-header-menu').length > 0 && $('.b-burger').length > 0 ) {
		$('.b-burger').on('click', function() {
			$('.b-header__menubox').toggleClass('b-header__menubox_open');
		});
	}
	
	/**
	 * Owl Carousel
	 */
	$('.owl-carousel').owlCarousel({
	    items: 1,
	    loop:true,
	    dots: true,
	    autoHeight:true
	});
});