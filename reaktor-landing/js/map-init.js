ymaps.ready(init);
var myMap, myPlacemark, myPlacemark2, myPlacemark3;

function init(){     
    myMap = new ymaps.Map("map", {
        center: [54.990108,73.280575],
        zoom: 12,
        controls: [],
        suppressObsoleteBrowserNotifier: false
    });
    myPlacemark = new ymaps.Placemark([55.014608,73.377375], { 
    	hintContent: 'ул. 13-я Северная, 157', 
    	//balloonContent: 'Столица России' 
	}, {
		iconLayout: 'default#image',
        iconImageHref: '/img/marker.png',
        iconImageSize: [36, 62],
	});
    
    myPlacemark2 = new ymaps.Placemark([54.960431,73.401638], { 
        hintContent: 'ул. Ипподромная, 2б', 
        //balloonContent: 'Столица России' 
    }, {
		iconLayout: 'default#image',
        iconImageHref: '/img/marker.png',
        iconImageSize: [36, 62],
	});
    myPlacemark3 = new ymaps.Placemark([54.983587,73.28483], { 
        hintContent: 'ул. Ватутина, 17', 
        //balloonContent: 'Столица России' 
    }, {
		iconLayout: 'default#image',
        iconImageHref: '/img/marker.png',
        iconImageSize: [36, 62],
	});

    myMap.geoObjects.add(myPlacemark);
    myMap.geoObjects.add(myPlacemark2);
    myMap.geoObjects.add(myPlacemark3);
}