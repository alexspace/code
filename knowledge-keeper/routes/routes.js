'use strict';
let pages = require('../controllers/pages');
let books = require('../controllers/books');

module.exports = function(app) {
    app.get('/', pages.index);
    app.get('/books', books.list);
    app.get('/in-process', books.inProcess);
    app.get('/statistics', pages.statistics);
    app.get('/plan', pages.plan);
    app.get('/books/add', books.addNewBook);
    app.get('/books/:name', books.item);
    app.get('*', pages.error404);
}
