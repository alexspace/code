'use strict';

exports.index = (req, res) => {
    res.render('index', {
        title: 'Main page',
        message: 'All main information',
    });
};

exports.statistics = (req, res) => {
    res.render('statistics', {
        title: 'Statistics',
        message: 'Do it with D3.js',
    });
};

exports.plan = (req, res) => {
    res.render('plan', {
        title: 'Plan',
        message: 'Backbone or React TODO list',
    });
};

exports.error404 = (req, res) => {
    res.sendStatus(404);
};
