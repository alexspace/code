'use strict';

let Book = require('../models/book');

exports.list = (req, res) => {
    let allbooks = Book.findAll();
    res.render('books', {
        title: 'Read Books',
        message: 'You are on books page!',
        meta: {
            description: 'My cool app!',
            keywords: 'One Two Three'
        },
        books: [
            {
                name: 'Новая большая книга CSS',
                author: 'Девид Макфарланд'
            },
            {
                name: 'Семь баз данных за семь недель',
                author: 'Редмонд Э., Уилсон Д.'
            },
            {
                name: 'Чистый код. Создание, анализ и рефакторинг',
                author: 'Р. Мартин'
            },
        ]
    });
};

exports.item = (req, res) => {
    let book = Note.find(req.params.name);
    res.send(book);
};

exports.inProcess = (req, res) => {
    res.render('in-process', {
        title: 'What I\'m reading',
        message: 'You are on books page!',
        meta: {
            description: 'My cool app!',
            keywords: 'One Two Three'
        },
        books: [
            {
                name: 'Экстримальное программирование',
                author: 'Кент Бек'
            },
            {
                name: 'Программист прогматик',
                author: 'Эндрю Хант, Дэвид Томас'
            },
            {
                name: 'Путь IT-менеджера',
                author: 'Перерва А., Еранов С., Иванова В., Сергеев С.'
            },
        ]
    });
};

exports.addNewBook = (req, res) => {
    res.render('add-new-book', {
        title: 'Add new read book',
        message: 'Backbone or React TODO list',
    });
};
