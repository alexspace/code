'use strict';

let path = require('path');
let express = require('express');
let app = express();
let hbs = require('express-hbs');
let morgan = require('morgan');

// MONGO PART IN DEVELOPMENT RIGHT NOW
// let db = require('./db');
// Connect to Mongo on start
// db.connect('mongodb://localhost:27017/knowledge-keeper', function(err) {
//   if (err) {
//     console.log('Unable to connect to Mongo.');
//     console.log(err);
//     process.exit(1);
//   } else {
//     app.listen(3000, function() {
//       console.log('Listening on port 3000...');
//     });
//   }
// });

// Logs all request
app.use(morgan('dev'));

app.use(express.static(__dirname + '/assets/'));
app.use('/books/', express.static(__dirname + '/assets/'));

app.set('view engine', 'hbs');
app.set('views', __dirname + '/views');
app.engine('hbs', hbs.express4({
  partialsDir: __dirname + '/views/partials',
  defaultLayout: __dirname + '/views/layout.hbs'
}));

require('./routes/routes')(app);

app.listen(8080);
